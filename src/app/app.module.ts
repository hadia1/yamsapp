import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PassDataService } from './pass-data.service';
import { ReserveComponent } from './reserve/reserve.component';
import { Joueur1Component } from './joueur1/joueur1.component';
import { Joueur2Component } from './joueur2/joueur2.component';

@NgModule({
  declarations: [
    AppComponent,
    ReserveComponent,
    Joueur1Component,
    Joueur2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [PassDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
