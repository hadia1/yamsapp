import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PassDataService { //service qui permet de passer les données entre le composant parents et les composant fils

  constructor() {}
  listede :number[]=[]; //liste des dé qu'on a réservé
  score=0;
  compteur!:number;
  nb!:number;
  un=0;
  deux=0;
  trois=0;
  quatre=0;
  cinq=0;
  six=0;

  recupData(de:number){  //fonction recupData permet de récuperer le dé et l'ajouter à la liste des dé
    console.log('RecupData : ' + de);
    this.listede.push(de);
  }
  //on calcul le nbr de tours pour vider la liste une fois celui ci dépasse 3
  calculNbrTours(nbrtrs: number){
    if (nbrtrs%3==0){
      this.listede=[];
    }
  }
  getDe() { //fonction getDe retourne la liste des dé récuperée
    return this.listede;
  }
  calculScore()
  {
    for (let i = 0; i < this.listede.length;i++)
    {
      this.score = this.score + Number(this.listede[i]);
      console.log(this.score);
    }
    return this.score;
  }

  getName1(name:any) {
    return name;
  }
  
  calculSpeciaux(choix:any){
    this.score=0;
    this.compteur=0;
    for (let i = 0; i < this.listede.length;i++)
    {
      if(this.listede[0]==this.listede[i]){
        this.compteur=this.compteur+1;
        console.log(this.compteur);
      }
    }
    if(choix="brelan" && this.compteur>=3){
      for (let i = 0; i < this.listede.length;i++)
      { 
        this.score = this.score + Number(this.listede[i]);
      }
      console.log("brelan score: ",this.score) ;
    }
    else if (choix="carre" && this.compteur>=4){
      for (let i = 0; i < this.listede.length;i++)
      { 
        this.score = this.score + Number(this.listede[i]);
      }
    } 
    else if (choix="yams" && this.compteur==5){
      for (let i = 0; i < this.listede.length;i++)
      { 
        this.score = 50;
      }
    }
    else{
      this.score=0;
    }
    return this.score;
  }
  calculNombre(nb:any){
    this.compteur=0;
    console.log("initialise compteur : ", this.compteur)
    
    for (let i = 0; i < this.listede.length;i++)
    {
      console.log("valuer de nb: ",this.nb)
      if(this.listede[i]==1){
        this.compteur= this.compteur+1;
        console.log("compteur de 1 : ", i)
      }
      else if(this.listede[i]==2)  {
        this.compteur++;
      }
      else if(this.listede[i]==3)  {
        this.compteur++;
      }
      else if(this.listede[i]==4)  {
        this.compteur++;
      }
      else if(this.listede[i]==5)  {
        this.compteur++;
      }
      else if(this.listede[i]==6)  {
        this.compteur++;
      }
    }
    this.score=this.compteur*nb;
    
    console.log("score finale : ",this.score);
    return this.score;
  }
calculSuite(choix:any){
    this.score=0;
    for (let i = 0; i < this.listede.length;i++)
    {
      if(this.listede[i]==1 && this.un==0){
        this.un=1;
      }
      else if(this.listede[i]==2 && this.deux==0)  {
        this.deux=1;
      }
      else if(this.listede[i]==3 && this.trois==0)  {
        this.trois=1;
      }
      else if(this.listede[i]==4 && this.quatre==0)  {
        this.quatre=1;
      }
      else if(this.listede[i]==5 && this.cinq==0)  {
        this.cinq=1;
      }
      else if(this.listede[i]==6 && this.six==0)  {
        this.six=1;
      }
    }
    if(choix="pSuite"){
      if(this.un==1 && this.deux==1 && this.trois==1 && this.quatre==1 ){
        this.score=15;
      } 
      else if( this.deux==1 && this.trois==1 && this.quatre==1 && this.cinq==1 ){
        this.score=15;
      } 
      else if(this.trois==1 && this.quatre==1 && this.cinq==1 && this.six==1){
        this.score=15;
      }
    }
    else if(choix="gSuite"){
      if( this.un==1 && this.deux==1 && this.trois==1 && this.quatre==1 && this.cinq==1 ){
        this.score=20;
      } 
      else if(this.deux==1 && this.trois==1 && this.quatre==1 && this.cinq==1 && this.six==1){
        this.score=20;
      }
    }
    else{
      this.score=0;
    }
    return this.score;
    console.log(this.score);
  }
  
}
